#include "mrfio.h"
#include <set>

EdgeIndexVector EdgeIndexImporter::import(istream& is) {
    size_t idx1, idx2;
    EdgeIndexVector eidxs;
    while (is >> idx1) {
        is >> idx2;
        idx1--;
        idx2--;
        eidxs.push_back(EdgeIndex(idx1, idx2));
    }
    return eidxs;
}

void MRFExporter::export_model(const MRF& model, ostream& os, bool c2) {

  const static bool debug = false;
  
  const string sep = "\t";
  Eigen::IOFormat fmt(4, Eigen::DontAlignCols, sep, sep);
  size_t n = model.get_length();
  string seq = model.get_seq();
  size_t k = model.get_num_var();
  EdgeIndexVector edge_idxs = model.get_edge_idxs();
  size_t n2 = edge_idxs.size();
  // Writing modeling log
  size_t fmtnum = model.get_fmtnum();
  os.write((char*)&fmtnum, sizeof(size_t));
  float neff = model.get_neff();
  os.write((char*)&neff, sizeof(float));
  //string ver = VERSION;
  // Writing sequence
  if (c2 && seq.size() % 2)
    {
      std::cerr << "Sequence length not even in C2 mode\n";
      exit(1);
    }
  if (c2) {
    if (debug) {
      std::cout << "Length: " << n << " reduced to " << n/2 << endl; 
      std::cout << "Sequence: " << seq << " reduced to ";
    }
    n = n/2;
    seq.resize(seq.size()/2);
    if (debug) std::cout <<  seq << endl;
  }
  
  os.write((char*)&n, sizeof(size_t));
  os.write(seq.c_str(), seq.size() + 1);
  // Writing MRF architecture
  // if C2 compute edge numbers in symmetrized MRF
  std::set<pair<size_t, size_t>> edges;
  vector<pair<size_t, size_t>> ledges;
  if (c2) {
    for (EdgeIndexVector::const_iterator pos = edge_idxs.begin(); pos != edge_idxs.end(); ++pos) {

      // inside the final variables
      if ((pos->idx1 < n) && (pos->idx2 <n)) {
	if (debug)
	  std::cout << "Edge " << pos->idx1 << " " << pos->idx2;
	auto p = make_pair(pos->idx1, pos-> idx2);
	if (edges.count(p) == 0) {
	  edges.insert(p);
	  ledges.push_back(p);
	  if (debug) std::cout << " added as is";
	}
	if (debug) std::cout << endl;
      }
      
      // entirely on the other unit: add to first unit
      if ((pos->idx1 >= n) && (pos->idx2 >= n)) {
	if (debug)
	  std::cout << "Edge " << pos->idx1 << " " << pos->idx2;
	auto p = make_pair(pos->idx1-n, pos-> idx2-n);
	if (edges.count(p) == 0) {
	  edges.insert(p);
	  ledges.push_back(p);
	  if (debug) std::cout << " added as " << pos->idx1-n << " " << pos->idx2-n << endl;
	}
	if (debug) std::cout << endl;
      }
      
	// between unit interactions A B: add to first unit if different positions
	if ((pos->idx1 < n) && (pos->idx2 >= n) && ((pos->idx2 - n) != (pos->idx1))) {
	if (debug)
	  std::cout << "Edge " << pos->idx1 << " " << pos->idx2;
	  size_t p1 = pos->idx1;
	  size_t p2 = pos-> idx2-n;
	  if (p2 < p1) std::swap(p1,p2);
	  auto p = make_pair(p1, p2);
	  if (edges.count(p) == 0) { 
	    edges.insert(p);
	    ledges.push_back(p);
	    if (debug) std::cout << " added as " << pos->idx1 << " " << pos->idx2-n << endl;
	  }
	  if (debug) std::cout << endl;
	}
	
	// should not happen if edges are such that  v1 < v2 TSX
	// between unit interactions B A: add to first unit if different positions
	if ((pos->idx1 >= n) && (pos->idx2 < n) && ((pos->idx1 - n) != pos->idx2)) {
	  if (debug)
	    std::cout << "!! => Edge " << pos->idx1 << " " << pos->idx2;
	  size_t p1 = pos->idx1-n;
	  size_t p2 = pos-> idx2;
	  if (p2 < p1) std::swap(p1,p2);
	  auto p = make_pair(p1, p2);
	  if (edges.count(p) == 0) { 
	    edges.insert(p);
	    ledges.push_back(p);
	    if (debug) std::cout << " added as " << pos->idx1-n << " " << pos->idx2 << endl;
	  }
	  if (debug) std::cout << endl;
	}
    }
      
      size_t edgenbr = edges.size();
      os.write((char*)&edgenbr, sizeof(size_t));

      for (auto const &e : ledges) {
	if (debug) std::cout << "Wrote edge " << e.first << " " << e.second << endl;
	os.write((char*)&(e.first), sizeof(size_t));
	os.write((char*)&(e.second), sizeof(size_t));
      }
    }
    else
      {
	os.write((char*)&n2, sizeof(size_t));
      
	for (EdgeIndexVector::const_iterator pos = edge_idxs.begin(); pos != edge_idxs.end(); ++pos) {
	  os.write((char*)&(pos->idx1), sizeof(size_t));
	  os.write((char*)&(pos->idx2), sizeof(size_t));
	}
      }
    // Writing sequence profile
    // c2: Nothing to do if the alignment has been properly symmetrized
    os.write((char*)model.get_psfm().data(), n * k * sizeof(float));

    // Writing node weights
    // c2: must also include some edge weights (AB terms on symmetric positions)
    if (c2) {
      for (size_t i = 0; i < n; ++i) {
	auto datai = model.get_node(i).get_weight().data();
	auto datai2 = model.get_node(i+n).get_weight().data();
	auto datab = model.get_edge(i, i+n).get_weight().data();
	if (debug) std::cout << "Writing node " << i << endl; 
	for (size_t j = 0; j < k; ++j) {
	  float nw = datai[j];
	  nw += datai2[j];
	  nw += datab[j+k*j];
	  os.write((char*)&nw, sizeof(float));
	}
      }
    }
    else {
      for (size_t i = 0; i < n; ++i)
	os.write((char*)model.get_node(i).get_weight().data(), k * sizeof(float));
    }	    
      
    // Writing edge weight
    if (c2) {// Stupid loops !
      for (auto const &e : ledges) {
	size_t v1 = e.first;
	size_t v2 = e.second;
	float weights[k*k];
	if (debug)
	  std::cout << "Weights for " << v1 << " " << v2 << "\n";

	for (EdgeIndexVector::const_iterator pos = edge_idxs.begin(); pos != edge_idxs.end(); ++pos){
	  for (size_t i = 0; i < k*k; ++i) weights[i] = 0.0;
	  // inside the final variables
	  if ((pos->idx1 == v1) && (pos->idx2 == v2)) {
	    if (debug) std::cout << "Adding " << pos->idx1 << " " << pos->idx2 << endl;
	    for (size_t i = 0; i < k*k; ++i) weights[i] += model.get_edge(v1,v2).get_weight().data()[i];
	  }
	  
	  // entirely on the other unit
	  if ((pos->idx1 == v1 + n) && (pos->idx2 == v2 + n)) {
	    if (debug) std::cout << "Adding " << pos->idx1 << " " << pos->idx2 << endl;
	    for (size_t i = 0; i < k*k; ++i) weights[i] += model.get_edge(pos->idx1, pos->idx2).get_weight().data()[i];	  
	  }
	  
	  // between unit interactions A B
	  if ((pos->idx1 == v1) && (pos->idx2 == v2+n)) {
	    if (debug) std::cout << "Adding " << pos->idx1 << " " << pos->idx2 << endl;
	    for (size_t i = 0; i < k*k; ++i) weights[i] += model.get_edge(pos->idx1, pos->idx2).get_weight().data()[i];	  
	  }
	  
	  // should not happen if edges are such that  v1 < v2 TSX
	  // between unit interactions B A: add to first unit if different positions
	  if ((pos->idx1 == v1+n) && (pos->idx2 == v2)) {
	    if (debug) std::cout << "Adding " << pos->idx1 << " " << pos->idx2 << endl;
	    for (size_t i = 0; i < k*k; ++i)
	      weights[i] += model.get_edge(pos->idx1, pos->idx2).get_weight().data()[i];
	  }
	  os.write((char*)weights, k * k * sizeof(float));
	}
      }
    }
    else {
      for (EdgeIndexVector::const_iterator pos = edge_idxs.begin(); pos != edge_idxs.end(); ++pos)
        os.write((char*)model.get_edge(pos->idx1, pos->idx2).get_weight().data(), k * k * sizeof(float));
    }
}

MRF MRFImporter::import_model(istream& is, const Alphabet& abc) {
    // Reading logging information
    size_t fmtnum;
    is.read((char*)&fmtnum, sizeof(size_t));
    float neff;
    is.read((char*)&neff, sizeof(float));
    // Reading sequence
    size_t n, n2;
    string seq;
    is.read((char*)&n, sizeof(size_t));
    std::getline(is, seq, '\0');
    // Reading MRF architecture
    is.read((char*)&n2, sizeof(size_t));
    EdgeIndexVector edge_idxs;
    size_t idx1, idx2;
    for (size_t i = 0; i < n2; ++i) {
        is.read((char*)&idx1, sizeof(size_t));
        is.read((char*)&idx2, sizeof(size_t));
        edge_idxs.push_back(EdgeIndex(idx1, idx2));
    }
    MRF model(seq, abc, &edge_idxs);
    model.set_fmtnum(fmtnum);
    model.set_neff(neff);
    size_t k = model.get_num_var();
    // Reading profile
    MatrixXf m(n, k);
    is.read((char*)m.data(), n * k * sizeof(float));
    model.set_psfm(m);
    // Reading node weight
    VectorXf v(k);
    for (size_t i = 0; i < n; ++i) {
        is.read((char*)v.data(), k * sizeof(float));
        model.get_node(i).set_weight(v);
    }
    // Reading edge weight
    m.resize(k, k);
    for (EdgeIndexVector::const_iterator pos = edge_idxs.begin(); pos != edge_idxs.end(); ++pos) {
        is.read((char*)m.data(), k * k * sizeof(float));
        model.get_edge(pos->idx1, pos->idx2).set_weight(m);
    }
    return model;
}
